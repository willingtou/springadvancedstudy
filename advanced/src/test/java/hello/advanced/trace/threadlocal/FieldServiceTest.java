package hello.advanced.trace.threadlocal;

import hello.advanced.trace.threadlocal.code.FieldService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

@Slf4j
public class FieldServiceTest {

    private FieldService fieldService = new FieldService();

    @Test
    public void field() throws Exception
    {
        log.info("main start");
        Runnable userA = ()-> fieldService.logic("userA"),
                userB =  ()-> fieldService.logic("userB");

        Thread threadA = new Thread(userA),
                threadB = new Thread(userB);

        threadA.setName("thread-A");
        threadB.setName("thread-B");

        threadA.start();
//        sleep(2000); //동시성 문제 발생 하지 않는 테스트.
        sleep(100); //동시성 문제 발생 O
        threadB.start();
        sleep(3000); //메인 스레드 종료 대기 (스레드B 결과도 보기위해 )

    }

    private void sleep(int ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
