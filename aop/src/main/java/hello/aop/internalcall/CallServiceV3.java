package hello.aop.internalcall;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * 구조를 변경 (분리)
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class CallServiceV3 {

    private final InternalService internalService;

    public void external(){

        log.info("call external service");
        internalService.internal();
    }


    @Slf4j
    @Component
    static class InternalService{
        public void internal(){
            log.info("call internal service");
        }
    }
}
